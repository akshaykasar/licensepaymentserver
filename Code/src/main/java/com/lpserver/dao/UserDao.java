package com.lpserver.dao;

import com.lpserver.bean.User;

/**
 * 
 * @author akshay.kasar
 *
 */
public interface UserDao {
	public User getUserById(int userId);
	public User getUserByEmailId(String email);
	public void save(User user);
	public void deleteUserById(int userId);
}
