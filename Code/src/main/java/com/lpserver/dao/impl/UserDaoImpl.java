package com.lpserver.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lpserver.bean.User;
import com.lpserver.dao.AbstractDao;
import com.lpserver.dao.UserDao;

/**
 * 
 * @author akshay.kasar
 *
 */
@Repository		//do this always for a dao it is better than @Component
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public User getUserById(int userId) {
		User user = getByKey(userId);
		return user;
	}

	@Override
	public User getUserByEmailId(String email) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("email", email));
		User user = (User) crit.uniqueResult();
		return user;
		
	}

	@Override
	public void save(User user) {
		persist(user);		
	}

	@Override
	public void deleteUserById(int userId) {
		User user = getByKey(userId);
		delete(user);
	}

}
